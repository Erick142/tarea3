package tarea.tarea;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static tarea.tarea.ValidadorApellidos.*;
import static tarea.tarea.ValidadorEdad.esUnaEdadValida;
import static tarea.tarea.ValidadorNombre.esUnNombreValido;
import static tarea.tarea.ValidadorNumeroTelefono.esUnTelefonoValido;
import static tarea.tarea.ValidadorRut.esUnRutValido;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TareaApplicationTests {

	@Test
	@DisplayName("Tests de nombre")
	void testDeNombre() {
		assertTrue(esUnNombreValido("Erick"));
		assertTrue(esUnNombreValido("Erick Demetrio"));

		assertFalse(esUnNombreValido("Erick Demetrio Demetrio"));
		assertFalse(esUnNombreValido("Erick@@@"));
		assertFalse(esUnNombreValido(""));
		assertFalse(esUnNombreValido("Ercik123122asd"));
	}
	@Test
	@DisplayName("Test apellido paterno")
	void testDeApellidoPaterno(){
		assertTrue(esUnApellidoPaternoValido("Martinez"));

		assertFalse(esUnApellidoPaternoValido("Martinez Martinez"));
		assertFalse(esUnApellidoPaternoValido(""));
		assertFalse(esUnApellidoPaternoValido("           "));
		assertFalse(esUnApellidoPaternoValido("Maerin12 sacc"));
	}
	@Test
	@DisplayName("Test apellido materno")
	void testDeApellidoMaterno(){
		assertTrue(esUnApellidoMatenoValido("Fuentes"));

		assertFalse(esUnApellidoMatenoValido("Fuentes fuentes"));
		assertFalse(esUnApellidoMatenoValido("        "));
		assertFalse(esUnApellidoMatenoValido(""));
		assertFalse(esUnApellidoMatenoValido("Fue asd 2 @@"));
	}
	@Test
	@DisplayName("Test rut")
	void testDeRut(){
		assertTrue(esUnRutValido("21039680-2"));
		assertTrue(esUnRutValido("23955394-K"));
		assertTrue(esUnRutValido("23955394-k"));

		assertFalse(esUnRutValido("12345678-9"));
		assertFalse(esUnRutValido("1231312312312323"));
		assertFalse(esUnRutValido(""));
		assertFalse(esUnRutValido("         "));
		assertFalse(esUnRutValido("asdasdasd"));
	}
	@Test
	@DisplayName("Test numero telefono")
	void testDeTelefono(){
		assertTrue(esUnTelefonoValido("132132232"));

		assertFalse(esUnTelefonoValido("1"));
		assertFalse(esUnTelefonoValido("1234567891"));
		assertFalse(esUnTelefonoValido("asdadsad"));
		assertFalse(esUnTelefonoValido(""));
	}
	@Test
	@DisplayName("Test edad")
	void testDeEdad(){
		assertTrue(esUnaEdadValida("20"));

		assertFalse(esUnaEdadValida("-10"));
		assertFalse(esUnaEdadValida("4000"));
		assertFalse(esUnaEdadValida("13123123"));
		assertFalse(esUnaEdadValida("asdasdasd"));
		assertFalse(esUnaEdadValida(""));
		assertFalse(esUnaEdadValida("   "));
	}

}
